import express from 'express'
import cors from 'cors'
import routes from './routes.js'

const port = process.env.PORT || 8000

const server = express()
server.use(cors())
server.use(express.json())
server.use(routes)
server.listen(port, () => console.log('> Server is running...'))
