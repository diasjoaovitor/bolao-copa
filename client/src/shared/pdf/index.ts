import { TDocumentDefinitions } from "pdfmake/interfaces"
import { columns } from "../states"
import { TData } from "../types"

const c = columns.map(({ headerName }) => headerName) as string[]

export const pdf = (data: TData[]): TDocumentDefinitions => {
  const d = data.map(({ 
    jogo, data, hora, local, grupo, mandante, placarMandante, x, placarVisitante, visitante
  }) => [ 
    jogo, data, hora, local, grupo, mandante, placarMandante, x, placarVisitante, visitante 
  ] )
  return {
    content: [
      {
        table: {
          body: [
            c,
            ...d
          ]
        }
      }
    ]
  }
}