import { months } from "../states";
import { TData, TResponseData } from "../types";

const formatZero = (value: Number) => value < 10 ? `0${value}` : value

const convertExcelDay = (day: Number) => {
  return new Date(Math.round((Number(day) - 25569) * 86400 * 1000))
}

const convertExcelHours = (hours: Number) => {
  return 24 * Number(hours)
}

export const formatData = (data: TResponseData): TData[] => {
  const d = data.map((array, index) => {
    if (array.length === 12 && index > 2) {
      const [ 
        _, __, 
        jogo, dia, hora, local, grupo, mandante, 
        ___, ____, _____, 
        visitante 
      ] = array
      if (typeof jogo !== "number") return
      const date = convertExcelDay(dia)
      const day = formatZero(date.getDate() + 1)
      const month = months[date.getMonth()]
      const hours = formatZero(convertExcelHours(hora))
      return {
        jogo, 
        data: `${day}/${month}`, 
        hora: `${hours}:00`, 
        local, 
        grupo, 
        mandante, 
        placarMandante: 0,
        x: 'X',
        placarVisitante: 0,
        visitante
      }
    }
  }).filter(d => d)
  return d as TData[]
}
