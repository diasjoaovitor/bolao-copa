import { useEffect, useState } from "react"
import pdfMake from 'pdfmake/build/pdfmake'
import pdfFonts from 'pdfmake/build/vfs_fonts'
import { formatData } from "../functions"
import { read } from "../services"
import { TData } from "../types"
import { pdf } from "../pdf"

export const useApp = () => {
  pdfMake.vfs = pdfFonts.pdfMake.vfs
  const [ data, setData ] = useState<TData[]>([])

  useEffect(() => {
    (async () => {
      const data = await read()
      const d = formatData(data)
      setData(d)
    })()
  }, [])

  const handleSubmit = () => {
    pdfMake.createPdf(pdf(data)).open()
  }

  return {
    data, handleSubmit
  }
}
