import { TResponseData } from "../types"

export const read = async (): Promise<TResponseData> => {
  const response = await fetch("http://localhost:8000")
  const [ { data } ] = await response.json()
  return data as TResponseData
}
