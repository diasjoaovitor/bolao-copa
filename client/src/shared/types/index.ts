export type TResponseData = any[][]

export type TData = {
  jogo: Number,
  data: string
  hora: string
  local: string
  grupo: string
  mandante: string
  placarMandante: number
  x: 'X'
  placarVisitante: number
  visitante: string
}
