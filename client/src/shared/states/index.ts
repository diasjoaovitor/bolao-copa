import { GridColDef } from "@mui/x-data-grid"

export const months = ['jan', 'fev', 'mar', 'abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez']

export const columns: GridColDef[] = [
  { field: 'jogo', headerName: 'J', align: 'center', headerAlign: 'center', width: 1 },
  { field: 'data', headerName: 'Data', align: 'center', headerAlign: 'center' },
  { field: 'hora', headerName: 'Hora', align: 'center', headerAlign: 'center' },
  { field: 'local', headerName: 'Local', align: 'center', headerAlign: 'center', width: 200 },
  { field: 'grupo', headerName: 'Grupo', align: 'center', headerAlign: 'center' },
  { field: 'mandante', headerName: '', align: 'right', width: 130  },
  { field: 'placarMandante', headerName: '', align: 'center', width: 1, editable: true },
  { field: 'x', headerName: '', align: 'center', width: 1 },
  { field: 'placarVisitante', headerName: '', align: 'center', width: 1, editable: true },
  { field: 'visitante', headerName: '', width: 130 }
]
