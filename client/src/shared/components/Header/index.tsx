import { Stack, Typography } from '@mui/material'
import { useThemeContext } from '../../contexts'
import * as S from './style'

export const Header: React.FC = () => {
  const { theme } = useThemeContext()
  const backgroundColor = theme.palette.background.paper

  return (
    <Stack component="header" sx={{ ...S.header, backgroundColor }}>
      <Typography component="h1" variant="h5">
        Bolão da Copa
      </Typography>
      <Typography component="a" variant="h6" color="gray" href="https://gitlab.com/diasjoaovitor/bolao-copa" target="_blank">
        GitLab
      </Typography>
    </Stack>
  )
} 
