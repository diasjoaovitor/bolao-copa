import { SxProps, Theme } from "@mui/material"

export const main: SxProps<Theme> = {
  width: '100%',
  padding: 2,
  '& .MuiDataGrid-iconSeparator': {
    display: 'none'
  },
  '& button': {
    marginTop: 2
  }
}

