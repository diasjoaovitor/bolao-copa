import { Box, Button } from '@mui/material'
import { DataGrid } from '@mui/x-data-grid'
import { columns } from '../../states'
import { TData } from '../../types'
import * as S from './style'

type Props = {
  data: TData[]
  handleSubmit(): void
}

export const Main: React.FC<Props> = ({ data, handleSubmit }) => {
  const rows = data.map((data, index) => ({ ...data, id: index }))
  return (
    <Box component="main" sx={S.main}>
      <DataGrid
        rows={rows}
        columns={columns}
        pageSize={6}
        autoHeight
        disableSelectionOnClick
        disableColumnFilter
        disableColumnMenu
      />
      <Button variant="contained" onClick={handleSubmit}>
        Salvar Aposta
      </Button>
    </Box>
  )
} 
